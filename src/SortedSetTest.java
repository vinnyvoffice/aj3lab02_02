import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

public class SortedSetTest {

	final Product macbookpro = new Product(1, "Apple MacBook Pro");
	final Product inspiron = new Product(2, "Dell Inspiron");
	final Product vostro = new Product(2, "Dell Vostro");
	final Product xps = new Product(2, "Dell XPS");

	@Test
	public void shouldAddInspiron() {
		List<Product> notebooks = Arrays.asList(macbookpro, inspiron, vostro);
		SortedSet<Product> sortedNotebooks = sortById(notebooks);
		Assert.assertEquals("Dell Inspiron", sortedNotebooks.last().getName());
	}

	@Test
	public void shouldAddVostro() {
		List<Product> notebooks = Arrays.asList(macbookpro, vostro, inspiron);
		SortedSet<Product> sortedNotebooks = sortById(notebooks);
		Assert.assertEquals("Dell Vostro", sortedNotebooks.last().getName());
	}

	@Test
	public void shouldAddXPS() {
		List<Product> notebooks = Arrays.asList(macbookpro, xps, vostro, inspiron);
		SortedSet<Product> sortedNotebooks = sortById(notebooks);
		Assert.assertEquals("Dell XPS", sortedNotebooks.last().getName());
	}

	private static SortedSet<Product> sortById(List<Product> notebooks) {
		Comparator<Product> sortById = (n1, n2) -> n1.getId() - n2.getId();
		SortedSet<Product> sortedNotebooks = new TreeSet<>(sortById);
		sortedNotebooks.addAll(notebooks);
		return sortedNotebooks;
	}

}

class Product {

	private int id;
	private String name;

	public Product(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}